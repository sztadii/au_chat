var header;

header = new function () {

  //catch DOM
  var $el;
  var $flex;
  var $documentTop;

  //bind events
  $(document).ready(function () {
    init();
  });

  $(window).scroll(function () {
    setTimeout(function () {
      $documentTop = $(document).scrollTop();

      if($documentTop > 150) {
        $flex.addClass('-scrolled');
      } else {
        $flex.removeClass('-scrolled');
      }
    });
  });

  //private functions
  var init = function () {
    $el = $(".header");
    $flex = $el.find('.header__flex');
  };
};